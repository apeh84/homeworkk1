import java.util.Arrays;

public class Balls {

   enum Color { green, red };

   public static void main (String[] param) {
      // for debugging\
      //System.out.println(Arrays.toString(reorder(new String[]{"green","red","red","green", "red"})));

   }

   public static String[] reorder (String[] balls) {
      int redBallCount = 0;
      //System.out.println(balls.length);

      // count how many red balls there is
      for (int i = 0; i < balls.length; i++) {

         if (balls[i] == "red" ){
            redBallCount++;
         }
      }


      // If all balls are red or green then just return
      if (redBallCount == balls.length) {
         return balls;
      } else if (redBallCount == 0) {
         return balls;
      }

      // replace the contents of the array with new colors that are in correct order
      for (int i = 0; i <redBallCount; i++) {
         balls[i] = "red";
      }
      System.out.println(redBallCount);
      for (int i = redBallCount; i < balls.length; i++) {
         balls[i] = "green";
      }
      return balls;
   }
}



